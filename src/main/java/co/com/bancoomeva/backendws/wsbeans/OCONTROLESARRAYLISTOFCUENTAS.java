
package co.com.bancoomeva.backendws.wsbeans;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para O_CONTROLES_ARRAYLIST_OF_CUENTAS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="O_CONTROLES_ARRAYLIST_OF_CUENTAS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="o_ESTADO" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="o_FEULMO" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="o_HOULMO" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="o_IDTRAN" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="o_MONDIA" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="o_MONOPE" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="o_NOMTRA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="o_USULMO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "O_CONTROLES_ARRAYLIST_OF_CUENTAS", propOrder = {
    "oestado",
    "ofeulmo",
    "ohoulmo",
    "oidtran",
    "omondia",
    "omonope",
    "onomtra",
    "ousulmo"
})
public class OCONTROLESARRAYLISTOFCUENTAS {

    @XmlElement(name = "o_ESTADO", required = true)
    protected BigDecimal oestado;
    @XmlElement(name = "o_FEULMO", required = true)
    protected BigDecimal ofeulmo;
    @XmlElement(name = "o_HOULMO", required = true)
    protected BigDecimal ohoulmo;
    @XmlElement(name = "o_IDTRAN", required = true)
    protected BigDecimal oidtran;
    @XmlElement(name = "o_MONDIA", required = true)
    protected BigDecimal omondia;
    @XmlElement(name = "o_MONOPE", required = true)
    protected BigDecimal omonope;
    @XmlElement(name = "o_NOMTRA", required = true)
    protected String onomtra;
    @XmlElement(name = "o_USULMO", required = true)
    protected String ousulmo;

    /**
     * Obtiene el valor de la propiedad oestado.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOESTADO() {
        return oestado;
    }

    /**
     * Define el valor de la propiedad oestado.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOESTADO(BigDecimal value) {
        this.oestado = value;
    }

    /**
     * Obtiene el valor de la propiedad ofeulmo.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOFEULMO() {
        return ofeulmo;
    }

    /**
     * Define el valor de la propiedad ofeulmo.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOFEULMO(BigDecimal value) {
        this.ofeulmo = value;
    }

    /**
     * Obtiene el valor de la propiedad ohoulmo.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOHOULMO() {
        return ohoulmo;
    }

    /**
     * Define el valor de la propiedad ohoulmo.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOHOULMO(BigDecimal value) {
        this.ohoulmo = value;
    }

    /**
     * Obtiene el valor de la propiedad oidtran.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOIDTRAN() {
        return oidtran;
    }

    /**
     * Define el valor de la propiedad oidtran.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOIDTRAN(BigDecimal value) {
        this.oidtran = value;
    }

    /**
     * Obtiene el valor de la propiedad omondia.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOMONDIA() {
        return omondia;
    }

    /**
     * Define el valor de la propiedad omondia.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOMONDIA(BigDecimal value) {
        this.omondia = value;
    }

    /**
     * Obtiene el valor de la propiedad omonope.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOMONOPE() {
        return omonope;
    }

    /**
     * Define el valor de la propiedad omonope.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOMONOPE(BigDecimal value) {
        this.omonope = value;
    }

    /**
     * Obtiene el valor de la propiedad onomtra.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getONOMTRA() {
        return onomtra;
    }

    /**
     * Define el valor de la propiedad onomtra.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setONOMTRA(String value) {
        this.onomtra = value;
    }

    /**
     * Obtiene el valor de la propiedad ousulmo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOUSULMO() {
        return ousulmo;
    }

    /**
     * Define el valor de la propiedad ousulmo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOUSULMO(String value) {
        this.ousulmo = value;
    }

}
