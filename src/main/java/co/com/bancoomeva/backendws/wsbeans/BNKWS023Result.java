
package co.com.bancoomeva.backendws.wsbeans;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para BNKWS023Result complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BNKWS023Result">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="o_CODRES" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="o_CONTROLES_ARRAY" type="{http://consultarcontrolesmontoscliente.wsbeans.iseries/xsd}O_CONTROLES_ARRAY"/>
 *         &lt;element name="o_LDIBCO" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="o_LTRBCO" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="o_MENRES" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BNKWS023Result", propOrder = {
    "ocodres",
    "ocontrolesarray",
    "oldibco",
    "oltrbco",
    "omenres"
})
public class BNKWS023Result {

    @XmlElement(name = "o_CODRES", required = true)
    protected BigDecimal ocodres;
    @XmlElement(name = "o_CONTROLES_ARRAY", required = true)
    protected OCONTROLESARRAY ocontrolesarray;
    @XmlElement(name = "o_LDIBCO", required = true)
    protected BigDecimal oldibco;
    @XmlElement(name = "o_LTRBCO", required = true)
    protected BigDecimal oltrbco;
    @XmlElement(name = "o_MENRES", required = true)
    protected String omenres;

    /**
     * Obtiene el valor de la propiedad ocodres.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOCODRES() {
        return ocodres;
    }

    /**
     * Define el valor de la propiedad ocodres.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOCODRES(BigDecimal value) {
        this.ocodres = value;
    }

    /**
     * Obtiene el valor de la propiedad ocontrolesarray.
     * 
     * @return
     *     possible object is
     *     {@link OCONTROLESARRAY }
     *     
     */
    public OCONTROLESARRAY getOCONTROLESARRAY() {
        return ocontrolesarray;
    }

    /**
     * Define el valor de la propiedad ocontrolesarray.
     * 
     * @param value
     *     allowed object is
     *     {@link OCONTROLESARRAY }
     *     
     */
    public void setOCONTROLESARRAY(OCONTROLESARRAY value) {
        this.ocontrolesarray = value;
    }

    /**
     * Obtiene el valor de la propiedad oldibco.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOLDIBCO() {
        return oldibco;
    }

    /**
     * Define el valor de la propiedad oldibco.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOLDIBCO(BigDecimal value) {
        this.oldibco = value;
    }

    /**
     * Obtiene el valor de la propiedad oltrbco.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOLTRBCO() {
        return oltrbco;
    }

    /**
     * Define el valor de la propiedad oltrbco.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOLTRBCO(BigDecimal value) {
        this.oltrbco = value;
    }

    /**
     * Obtiene el valor de la propiedad omenres.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOMENRES() {
        return omenres;
    }

    /**
     * Define el valor de la propiedad omenres.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOMENRES(String value) {
        this.omenres = value;
    }

}
