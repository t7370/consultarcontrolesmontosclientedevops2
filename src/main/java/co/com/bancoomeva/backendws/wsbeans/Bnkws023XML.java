
package co.com.bancoomeva.backendws.wsbeans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="args0" type="{http://consultarcontrolesmontoscliente.wsbeans.iseries/xsd}BNKWS023Input"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "args0"
})
@XmlRootElement(name = "bnkws023_XML")
public class Bnkws023XML {

    @XmlElement(required = true)
    protected BNKWS023Input args0;

    /**
     * Obtiene el valor de la propiedad args0.
     * 
     * @return
     *     possible object is
     *     {@link BNKWS023Input }
     *     
     */
    public BNKWS023Input getArgs0() {
        return args0;
    }

    /**
     * Define el valor de la propiedad args0.
     * 
     * @param value
     *     allowed object is
     *     {@link BNKWS023Input }
     *     
     */
    public void setArgs0(BNKWS023Input value) {
        this.args0 = value;
    }

}
