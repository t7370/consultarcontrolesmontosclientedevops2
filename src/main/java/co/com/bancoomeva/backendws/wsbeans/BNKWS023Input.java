
package co.com.bancoomeva.backendws.wsbeans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para BNKWS023Input complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BNKWS023Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="i_CODCAN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="i_CODTRN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="i_DATTIM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="i_DIREIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="i_MSGIDE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="i_NUMDOC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="i_TIPDOC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="i_USUARI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BNKWS023Input", propOrder = {
    "icodcan",
    "icodtrn",
    "idattim",
    "idireip",
    "imsgide",
    "inumdoc",
    "itipdoc",
    "iusuari"
})
public class BNKWS023Input {

    @XmlElement(name = "i_CODCAN", required = true)
    protected String icodcan;
    @XmlElement(name = "i_CODTRN", required = true)
    protected String icodtrn;
    @XmlElement(name = "i_DATTIM", required = true)
    protected String idattim;
    @XmlElement(name = "i_DIREIP", required = true)
    protected String idireip;
    @XmlElement(name = "i_MSGIDE", required = true)
    protected String imsgide;
    @XmlElement(name = "i_NUMDOC", required = true)
    protected String inumdoc;
    @XmlElement(name = "i_TIPDOC", required = true)
    protected String itipdoc;
    @XmlElement(name = "i_USUARI", required = true)
    protected String iusuari;

    /**
     * Obtiene el valor de la propiedad icodcan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getICODCAN() {
        return icodcan;
    }

    /**
     * Define el valor de la propiedad icodcan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setICODCAN(String value) {
        this.icodcan = value;
    }

    /**
     * Obtiene el valor de la propiedad icodtrn.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getICODTRN() {
        return icodtrn;
    }

    /**
     * Define el valor de la propiedad icodtrn.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setICODTRN(String value) {
        this.icodtrn = value;
    }

    /**
     * Obtiene el valor de la propiedad idattim.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDATTIM() {
        return idattim;
    }

    /**
     * Define el valor de la propiedad idattim.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDATTIM(String value) {
        this.idattim = value;
    }

    /**
     * Obtiene el valor de la propiedad idireip.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDIREIP() {
        return idireip;
    }

    /**
     * Define el valor de la propiedad idireip.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDIREIP(String value) {
        this.idireip = value;
    }

    /**
     * Obtiene el valor de la propiedad imsgide.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIMSGIDE() {
        return imsgide;
    }

    /**
     * Define el valor de la propiedad imsgide.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIMSGIDE(String value) {
        this.imsgide = value;
    }

    /**
     * Obtiene el valor de la propiedad inumdoc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINUMDOC() {
        return inumdoc;
    }

    /**
     * Define el valor de la propiedad inumdoc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINUMDOC(String value) {
        this.inumdoc = value;
    }

    /**
     * Obtiene el valor de la propiedad itipdoc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITIPDOC() {
        return itipdoc;
    }

    /**
     * Define el valor de la propiedad itipdoc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITIPDOC(String value) {
        this.itipdoc = value;
    }

    /**
     * Obtiene el valor de la propiedad iusuari.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIUSUARI() {
        return iusuari;
    }

    /**
     * Define el valor de la propiedad iusuari.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIUSUARI(String value) {
        this.iusuari = value;
    }

}
