
package co.com.bancoomeva.backendws.wsbeans;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the iseries.wsbeans.consultarcontrolesmontoscliente.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: iseries.wsbeans.consultarcontrolesmontoscliente.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Bnkws023Response }
     * 
     */
    public Bnkws023Response createBnkws023Response() {
        return new Bnkws023Response();
    }

    /**
     * Create an instance of {@link BNKWS023Result }
     * 
     */
    public BNKWS023Result createBNKWS023Result() {
        return new BNKWS023Result();
    }

    /**
     * Create an instance of {@link Bnkws023XML }
     * 
     */
    public Bnkws023XML createBnkws023XML() {
        return new Bnkws023XML();
    }

    /**
     * Create an instance of {@link BNKWS023Input }
     * 
     */
    public BNKWS023Input createBNKWS023Input() {
        return new BNKWS023Input();
    }

    /**
     * Create an instance of {@link Bnkws023XMLResponse }
     * 
     */
    public Bnkws023XMLResponse createBnkws023XMLResponse() {
        return new Bnkws023XMLResponse();
    }

    /**
     * Create an instance of {@link Bnkws023 }
     * 
     */
    public Bnkws023 createBnkws023() {
        return new Bnkws023();
    }

    /**
     * Create an instance of {@link OCONTROLESARRAY }
     * 
     */
    public OCONTROLESARRAY createOCONTROLESARRAY() {
        return new OCONTROLESARRAY();
    }

    /**
     * Create an instance of {@link OCONTROLESARRAYLISTOFCUENTAS }
     * 
     */
    public OCONTROLESARRAYLISTOFCUENTAS createOCONTROLESARRAYLISTOFCUENTAS() {
        return new OCONTROLESARRAYLISTOFCUENTAS();
    }

}
