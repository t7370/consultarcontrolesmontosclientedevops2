
package co.com.bancoomeva.backendws.wsbeans;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para O_CONTROLES_ARRAY complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="O_CONTROLES_ARRAY">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LIST_OF_CUENTAS" type="{http://consultarcontrolesmontoscliente.wsbeans.iseries/xsd}O_CONTROLES_ARRAYLIST_OF_CUENTAS" maxOccurs="unbounded"/>
 *         &lt;element name="o_RTNCON" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "O_CONTROLES_ARRAY", propOrder = {
    "listofcuentas",
    "ortncon"
})
public class OCONTROLESARRAY {

    @XmlElement(name = "LIST_OF_CUENTAS", required = true)
    protected List<OCONTROLESARRAYLISTOFCUENTAS> listofcuentas;
    @XmlElement(name = "o_RTNCON")
    protected long ortncon;

    /**
     * Gets the value of the listofcuentas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listofcuentas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLISTOFCUENTAS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OCONTROLESARRAYLISTOFCUENTAS }
     * 
     * 
     */
    public List<OCONTROLESARRAYLISTOFCUENTAS> getLISTOFCUENTAS() {
        if (listofcuentas == null) {
            listofcuentas = new ArrayList<OCONTROLESARRAYLISTOFCUENTAS>();
        }
        return this.listofcuentas;
    }

    /**
     * Obtiene el valor de la propiedad ortncon.
     * 
     */
    public long getORTNCON() {
        return ortncon;
    }

    /**
     * Define el valor de la propiedad ortncon.
     * 
     */
    public void setORTNCON(long value) {
        this.ortncon = value;
    }

}
