package co.com.bancoomeva.backendws.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

import co.com.bancoomeva.backendws.wsbeans.ObjectFactory;
import co.com.bancoomeva.constants.Constants;
import co.com.bancoomeva.enums.MediationService;
import co.com.bancoomeva.persistence.entity.Servicio;
import co.com.bancoomeva.restws.business.ILogMediacion;
import co.com.bancoomeva.restws.business.IServicio;
import co.com.bancoomeva.restws.model.ResponseModelConsultarControlesMontosCliente;
import co.com.bancoomeva.util.StaticContextAccessor;

/**
 * Esta clase obtiene la informacion y configuracion del cliente
 * webServiceTemplate
 * 
 * @author JhonCTorres
 * @version 28/12/2020
 *
 */
@Configuration
public class WsClientConfig {

	@Autowired
	private IServicio servicio;

	@Autowired
	private ILogMediacion logMediacionModel;

	@Autowired
	protected StaticContextAccessor staticContext;

	@Bean
	public WebServiceTemplate countriesWsResource() throws Exception {
		Servicio service = Servicio.builder().build();
		try {
			service = servicio.getServicio(MediationService.CONSULTAR_CONTROLES_MONTOS.getServiceCore());
		} catch (Exception e) {
			ResponseModelConsultarControlesMontosCliente responseWsModelFail = ResponseModelConsultarControlesMontosCliente.builder().build();
			responseWsModelFail.setMsjRespuesta(e.toString());
			logMediacionModel.traceLogResponseWs(responseWsModelFail, Constants.MEDIADOREKS);
		}
		String countryWebServiceUri = service.getWsdl();
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setPackagesToScan(ObjectFactory.class.getPackage().getName());
		WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
		webServiceTemplate.setDefaultUri(countryWebServiceUri);
		webServiceTemplate.setMarshaller(marshaller);
		webServiceTemplate.setUnmarshaller(marshaller);

		HttpComponentsMessageSender httpComponentsMessageSender = new HttpComponentsMessageSender();
		httpComponentsMessageSender.setReadTimeout(MediationService.CONSULTAR_CONTROLES_MONTOS.getTimeout());
		httpComponentsMessageSender.setConnectionTimeout(MediationService.CONSULTAR_CONTROLES_MONTOS.getTimeout());
		webServiceTemplate.setMessageSender(httpComponentsMessageSender);
		return webServiceTemplate;
	}
}
