package co.com.bancoomeva.constants;

public final class Constants {

	public static final String CONSULT_SUCCESSFUL = "Consulta Exitosa";
	public static final String INTERNAL_SERVER_ERROR = "Error Interno del Servidor";
	public static final String CONNECTION_REFUSED = "I/O error: Connection refused: no further information; nested exception is java.net.ConnectException: Connection refused: no further information";
	public static final String TIME_OUT = "I/O error: Connection";
	public static final String ERROR_CONNECTION = "error: Connection";
	public static final String INACTIVE_SERVICE = "system is attempting to access an inactive service";
	public static final String HOST_UNREACHABLE = "error: Host is unreachable (Host is unreachable); nested exception is java.net.NoRouteToHostException: Host is unreachable (Host is unreachable)";
	public static final String INTERNAL_ERROR_BACKEND = "500 Error interno del servidor";
	public static final String SERVICE_NOT_AVAILABLE = "Servicio no disponible";
	public static final String XML_STRUCTURE_ERROR = "Error estructura XML";
	public static final String CLIENT_NOT_FOUND = "Cliente no encontrado";
	public static final String RECORDS_NOT_FOUND = "No se encontraron registros para la consulta";
	public static final String ERROR_VALIDATING_FIELDS = "Error de estructura o validación de campos en el back";
	public static final String CONSULT_CREAR_CLIENTES_DATA = "Consultar la creacion de lo clientes cats creados";
	public static final String CONSULT_CREAR_CLIENTES_CATS_NOTE = "consultar informaci\u00f3n detallada de los Clientes Cats creado";
	public static final String REQUEST = "REQUEST";
	public static final String RESPONSE = "RESPONSE";
	public static final String MEDIADOREKS = "MEDIADOR-EKS";
	public static final String TAYLOR = "TAYLOR";
	public static final String ERROR = "ERROR";
	public static final String OK = "OK";
	public static final String TIMED = "timed";
	public static final String ERROR_503 = "503";
	public static final String ERROR_500 = "500";
	public static final String SUCCESS = "200";
	public static final String TRANSACTION_EXCEPTION = "CannotCreateTransactionException";
	public static final String PETITION_MEDIATION_INITIAL = "Petición a la mediación inicial ConsultarControlesMontosClientes";
	public static final String PETITION_CORE_TAYLOR = "Petición al CORE para verificar la consulta de controles montos clientes";
	public static final String PETITION_TAYLOR_RESPONSE = "Respuesta del CORE con los datos de consultarControlesMontosClientes";
	public static final String PETITION_MEDIATION_RESPONSE = "Respuesta de la mediación con los datos de consultarControlesMontosClientes";
	public static final String APPLICATION_NOT_FOUND = "Aplicaci\u00f3n que consume la mediaci\u00f3n no se encuentra registrada en base de datos";
	public static final String API_INFO_TITLE = "Documentaci\u00f3n API Servicio Mediaci\u00f3n E-Wallet: ";
	
	private Constants() {
		throw new IllegalStateException("Constants class");
	}
}
