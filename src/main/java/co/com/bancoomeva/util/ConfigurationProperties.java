package co.com.bancoomeva.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class ConfigurationProperties {

	private final int nivelLog;
	private final String serviceMediation;
	private final String serviceCore;
	private final int timeout;

	public ConfigurationProperties(@Value("${query.nivelLog}") int nivelLog,
			@Value("${query.servicio_mediacion}") String serviceMediation,
			@Value("${query.servicio_mediacion}") String serviceCore,
			@Value("${service.client_timeout}") int timeout) {
		this.nivelLog = nivelLog;
		this.serviceMediation = serviceMediation;
		this.serviceCore = serviceCore;
		this.timeout = timeout;
	}

}
