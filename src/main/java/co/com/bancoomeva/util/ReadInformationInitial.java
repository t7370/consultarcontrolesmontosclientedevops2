package co.com.bancoomeva.util;

import java.util.List;

import co.com.bancoomeva.persistence.dto.AplicacionDTO;
import co.com.bancoomeva.persistence.dto.MensajeRespuestaDTO;
import co.com.bancoomeva.persistence.dto.MensajeRespuestaObject;
import co.com.bancoomeva.persistence.entity.Aplicacion;
import lombok.Builder;

/**
 * Esta clase define la busqueda y carga de informacion correspondiente
 * 
 * @author JhonCTorres
 * @version 04/01/2021
 */
@Builder
public class ReadInformationInitial {
	
	/**
	 * Devuelve el id del mensaje encontrado
	 * @param nombreFlujo nombre del flujo
	 * @param nombreAplicacion nombre de la aplicacion
	 * @return id del mensaje
	 * @throws Exception
	 */
	public int getIdMensaje(String nombreFlujo, String nombreAplicacion) throws Exception {
		int idMensaje = 0;
		List<MensajeRespuestaDTO> listadoMensajes = null;		

		MensajeRespuestaObject mensajesResp = MensajeRespuestaObject.getMensajeRespuestaDTO(listadoMensajes);
		for (int i= 0; i<mensajesResp.getListadoMensajes().size();i++) {
			if(mensajesResp.getListadoMensajes().get(i).getNombreTipoFlujo().equals(nombreFlujo) && 
					mensajesResp.getListadoMensajes().get(i).getNombreAplicacion().equals(nombreAplicacion)) {
				idMensaje =mensajesResp.getListadoMensajes().get(i).getIdMensaje();
				break;
			}
		}		
		return idMensaje;
	}
	
	public boolean validarIdApp(String idApp) throws Exception {
		boolean validId = false;
		List<Aplicacion> listadoApps = null;
		int idApl = Integer.parseInt(idApp);
		AplicacionDTO appResp = AplicacionDTO.getAplicacionDTO(listadoApps);
		for (int i = 0; i < appResp.getListadoApps().size(); i++) {
			if (appResp.getListadoApps().get(i).getId() == (idApl)) {
				idApl = appResp.getListadoApps().get(i).getId();
				validId = true;
				break;
			}
		}
		return validId;
	}
	
	public int getIdAplicacion(String nombreAplicacion) throws Exception {
		int idApl = 0;
		List<Aplicacion> listadoApps = null;

		AplicacionDTO appResp = AplicacionDTO.getAplicacionDTO(listadoApps);
		for (int i = 0; i < appResp.getListadoApps().size(); i++) {
			if (appResp.getListadoApps().get(i).getNombreAplicacion().equals(nombreAplicacion)) {
				idApl = appResp.getListadoApps().get(i).getId();
				break;
			}
		}
		return idApl;
	}
	
	/**
	 * Devuelve el mensaje del response
	 * @param codRespuesta codigo de respuesta
	 * @param nombreFlujo nombre del flujo
	 * @param nombreAplicacion nombre de la aplicacion
	 * @return id del mensaje de respuesta
	 * @throws Exception
	 */
	public int getIdMensajeResponse(String codRespuesta, String nombreFlujo, String nombreAplicacion) throws Exception {
		int idMensaje = 0;
		List<MensajeRespuestaDTO> listadoMensajes = null;		

		MensajeRespuestaObject mensajesResp = MensajeRespuestaObject.getMensajeRespuestaDTO(listadoMensajes);
		for (int i= 0; i<mensajesResp.getListadoMensajes().size();i++) {
			if(mensajesResp.getListadoMensajes().get(i).getCodigoBack().equals(codRespuesta) &&
					mensajesResp.getListadoMensajes().get(i).getNombreTipoFlujo().equals(nombreFlujo) && 
					mensajesResp.getListadoMensajes().get(i).getNombreAplicacion().equals(nombreAplicacion)) {
				idMensaje =mensajesResp.getListadoMensajes().get(i).getIdMensaje();
				break;
			}
			if(mensajesResp.getListadoMensajes().get(i).getCodigo().equals(codRespuesta) &&
					mensajesResp.getListadoMensajes().get(i).getNombreTipoFlujo().equals(nombreFlujo) && 
					mensajesResp.getListadoMensajes().get(i).getNombreAplicacion().equals(nombreAplicacion)) {
				idMensaje =mensajesResp.getListadoMensajes().get(i).getIdMensaje();
				break;
			}
		}		
		return idMensaje;
	}
	
	/**
	 * Devuelve el objeto completo del mensaje de respuesta
	 * 
	 * @param codRespuesta codigo de respuesta
	 * @param nombreFlujo nombre del flujo
	 * @param nombreAplicacion nombre de la aplicacion
	 * @return objeto completo
	 * @throws Exception
	 */
	public MensajeRespuestaDTO getMensajeCompletoResponse(String codRespuesta, String nombreFlujo, String nombreAplicacion) throws Exception {
		MensajeRespuestaDTO mensajeCompleto = null;
		List<MensajeRespuestaDTO> listadoMensajes = null;		

		MensajeRespuestaObject mensajesResp = MensajeRespuestaObject.getMensajeRespuestaDTO(listadoMensajes);
		for (int i= 0; i<mensajesResp.getListadoMensajes().size();i++) {
			if(mensajesResp.getListadoMensajes().get(i).getCodigoBack().equals(codRespuesta) &&
					mensajesResp.getListadoMensajes().get(i).getNombreTipoFlujo().equals(nombreFlujo) && 
					mensajesResp.getListadoMensajes().get(i).getNombreAplicacion().equals(nombreAplicacion)) {
				mensajeCompleto = mensajesResp.getListadoMensajes().get(i);
				break;
			}
		}		
		return mensajeCompleto;
	}

}
