package co.com.bancoomeva.util;

import java.util.ArrayList;
import java.util.List;

import co.com.bancoomeva.backendws.wsbeans.BNKWS023Input;
import co.com.bancoomeva.backendws.wsbeans.Bnkws023;
import co.com.bancoomeva.backendws.wsbeans.Bnkws023Response;
import co.com.bancoomeva.backendws.wsbeans.OCONTROLESARRAYLISTOFCUENTAS;
import co.com.bancoomeva.constants.Constants;
import co.com.bancoomeva.persistence.dto.MensajeRespuestaDTO;
import co.com.bancoomeva.restws.model.ConsultarControlesMontosCliente;
import co.com.bancoomeva.restws.model.RequestModelConsultBranchCity;
import co.com.bancoomeva.restws.model.ResponseModelConsultarControlesMontosCliente;

public class MappingParameters {

	private MappingParameters() {
		throw new IllegalStateException("Utility class");
	}

	public static Bnkws023 mapMediationToTaylor(RequestModelConsultBranchCity requestWs) {
		Bnkws023 wsFacMov = new Bnkws023();

		BNKWS023Input wsfacInput = new BNKWS023Input();

		wsfacInput.setIMSGIDE(requestWs.getMessageId().trim());
		wsfacInput.setIDATTIM(requestWs.getInvokerDateTime().trim());
		wsfacInput.setIDIREIP(requestWs.getIpTransaccion().trim());
		wsfacInput.setICODTRN(requestWs.getCodTransaccion().trim());
		wsfacInput.setICODCAN(requestWs.getCanal().trim());
		wsfacInput.setITIPDOC(requestWs.getTipoDocumento().trim());
		wsfacInput.setINUMDOC(requestWs.getNumDocumento().trim());
		wsfacInput.setIUSUARI(requestWs.getUsuario().trim());

		wsFacMov.setArgs0(wsfacInput);

		return wsFacMov;
	}

	public static List<Bnkws023> mapListMediationToTaylor(List<RequestModelConsultBranchCity> listRequest) {

		List<Bnkws023> listReturned = new ArrayList<>();

		for (RequestModelConsultBranchCity item : listRequest) {
			listReturned.add(mapMediationToTaylor(item));
		}

		return listReturned;
	}

	public static ResponseModelConsultarControlesMontosCliente mapTaylorToMediation(Bnkws023Response bknws023returned) {

		ResponseModelConsultarControlesMontosCliente response = ResponseModelConsultarControlesMontosCliente.builder()
				.build();

		MensajeRespuestaDTO mensaje = Utils.consultMessage(String.valueOf(bknws023returned.getReturn().getOCODRES()),
				Constants.RESPONSE, Constants.TAYLOR);
		response.setCodRespuesta(mensaje.getCodigo());
		response.setMsjRespuesta(mensaje.getMensaje() + " - " + bknws023returned.getReturn().getOMENRES());
		response.setLimiteDiarioBco(String.valueOf(bknws023returned.getReturn().getOLDIBCO()));
		response.setLimiteTransaccionBco(String.valueOf(bknws023returned.getReturn().getOLTRBCO()));
		response.setListaconsultarControlesMontosCliente(
				mapListControlCuentas(bknws023returned.getReturn().getOCONTROLESARRAY().getLISTOFCUENTAS()));
		return response;

	}

	public static List<ConsultarControlesMontosCliente> mapListControlCuentas(
			List<OCONTROLESARRAYLISTOFCUENTAS> listControlCuentas) {

		List<ConsultarControlesMontosCliente> listaCuentas = new ArrayList<>();

		for (OCONTROLESARRAYLISTOFCUENTAS item : listControlCuentas) {
			listaCuentas.add(mapControlCuentas(item));
		}

		return listaCuentas;
	}

	public static ConsultarControlesMontosCliente mapControlCuentas(OCONTROLESARRAYLISTOFCUENTAS cuentas) {
		return ConsultarControlesMontosCliente.builder().transaccion(String.valueOf(cuentas.getOIDTRAN()))
				.nombre(cuentas.getONOMTRA()).montoMaxDia(String.valueOf(cuentas.getOMONDIA()))
				.montoMaxOperacion(String.valueOf(cuentas.getOMONOPE())).estado(String.valueOf(cuentas.getOESTADO()))
				.usuarioUltModificacion(cuentas.getOUSULMO()).fechaUltModificacion(String.valueOf(cuentas.getOFEULMO()))
				.horaUltModificacion(String.valueOf(cuentas.getOHOULMO())).build();
	}
}
