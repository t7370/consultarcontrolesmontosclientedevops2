package co.com.bancoomeva.util;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.bancoomeva.backendws.wsbeans.Bnkws023;
import co.com.bancoomeva.backendws.wsbeans.Bnkws023Response;
import co.com.bancoomeva.constants.Constants;
import co.com.bancoomeva.enums.MediationService;
import co.com.bancoomeva.persistence.entity.Logs;
import co.com.bancoomeva.restws.model.RequestModelConsultBranchCity;
import co.com.bancoomeva.restws.model.ResponseModelConsultarControlesMontosCliente;

public class BuildLog {

	private static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
	private static ObjectMapper obj = new ObjectMapper();
	private static Logs saveLog;
	private static String messageId;
	private static String user;
	private static String channel;
	private static String ip;
	private static int appId;
	private static int nivelLog;

	private BuildLog() {
		throw new IllegalStateException("Utility class");
	}

	private static void init() {
		if (nivelLog == 0)
			nivelLog = MediationService.CONSULTAR_CONTROLES_MONTOS.getNivelLog();
	}

	public static Logs buildLog(RequestModelConsultBranchCity requestModel, String flow) throws Exception {
		init();
		ReadInformationInitial infoMensaje = ReadInformationInitial.builder().build();
		messageId = requestModel.getMessageId();
		user = requestModel.getUsuario();
		channel = requestModel.getCanal();
		ip = requestModel.getIpTransaccion();
		appId = Integer.parseInt(requestModel.getIdAplicacion().trim());
		if (!infoMensaje.validarIdApp(requestModel.getIdAplicacion()))
			throw new SQLException(Constants.APPLICATION_NOT_FOUND);
		buildLogGlobal(requestModel, null, flow, Constants.MEDIADOREKS);
		return saveLog;
	}

	public static Logs buildLog(Bnkws023 requestTaylor, String flow) throws Exception {
		buildLogGlobal(requestTaylor, null, flow, Constants.TAYLOR);
		return saveLog;
	}

	public static Logs buildLog(Bnkws023Response response, String flow) throws Exception {
		buildLogGlobal(null, response, flow, Constants.TAYLOR);
		return saveLog;
	}

	public static Logs buildLog(ResponseModelConsultarControlesMontosCliente responseModel, String flow, String app) throws Exception {
		buildLogGlobal(null, responseModel, flow, app);
		return saveLog;
	}

	private static void buildLogGlobal(Object request, Object response, String flow, String app) throws Exception {

		ReadInformationInitial messageInfo = ReadInformationInitial.builder().build();
		int messagesId = messageInfo.getIdMensaje(response == null ? Constants.REQUEST : Constants.RESPONSE,
				app);
		String value = obj.writeValueAsString(response == null ? request : response);
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat(DATEFORMAT);
		String dateLog = format.format(date);

		saveLog = Logs.builder().build();
		saveLog.setMessageId(messageId);
		saveLog.setUsuario(user);
		saveLog.setCanal(channel);
		saveLog.setIp(ip);
		saveLog.setIdAplicacion(appId);
		saveLog.setIdNivelLog(nivelLog);
		saveLog.setPasoFlujo(flow);
		saveLog.setFecha(dateLog);
		saveLog.setContenidoMensaje(value);
		saveLog.setIdMensajes(messagesId);
	}

}
