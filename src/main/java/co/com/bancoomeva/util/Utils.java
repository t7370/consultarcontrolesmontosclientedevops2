package co.com.bancoomeva.util;

import co.com.bancoomeva.constants.Constants;
import co.com.bancoomeva.persistence.dto.MensajeRespuestaDTO;

public class Utils {
	
	private Utils() {
		throw new IllegalStateException("Utility class");
	}
	
	public static MensajeRespuestaDTO consultMessage(String cod, String flow, String app) {
		MensajeRespuestaDTO mensaje = new MensajeRespuestaDTO();
		try {
			ReadInformationInitial infoMensaje = ReadInformationInitial.builder().build();

			mensaje = infoMensaje.getMensajeCompletoResponse(cod, flow, app);

			if (mensaje == null)
				mensaje = infoMensaje.getMensajeCompletoResponse(Constants.ERROR_500, flow,
						app);
		} catch (Exception e) {

		}
		return mensaje;
	}
}
