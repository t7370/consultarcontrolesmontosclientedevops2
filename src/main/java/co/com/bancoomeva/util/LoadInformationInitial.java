package co.com.bancoomeva.util;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.com.bancoomeva.persistence.dto.AplicacionDTO;
import co.com.bancoomeva.persistence.dto.MensajeRespuestaDTO;
import co.com.bancoomeva.persistence.dto.MensajeRespuestaObject;
import co.com.bancoomeva.persistence.entity.Aplicacion;
import co.com.bancoomeva.restws.business.AplicacionImpl;
import co.com.bancoomeva.restws.business.ILogMediacion;
import co.com.bancoomeva.restws.business.IMessageResponseImpl;
import co.com.bancoomeva.restws.model.ResponseModelConsultarControlesMontosCliente;

/**
 * Esta clase define la lectura de registros que se encuentran en la BD
 * 
 * @author JhonCTorres
 * @version 04/01/2021
 */
@Component
public class LoadInformationInitial {

	@Autowired
	IMessageResponseImpl mensajesRespuesta;

	@Autowired
	ILogMediacion logMediacionModel;

	@Autowired
	AplicacionImpl aplicacion;
	
	@PostConstruct
	public void cargarMensajes() throws Exception {
		try {
			List<MensajeRespuestaDTO> listadoMensajes = new ArrayList<MensajeRespuestaDTO>();
			listadoMensajes = mensajesRespuesta.listaMensajesInterfaz();
			MensajeRespuestaObject.getMensajeRespuestaDTO(listadoMensajes);
		} catch (Exception e) {
			ResponseModelConsultarControlesMontosCliente responseWsModelFail = ResponseModelConsultarControlesMontosCliente.builder().build();
			responseWsModelFail.setMsjRespuesta(e.toString());
			logMediacionModel.traceFailMessagesMediation(responseWsModelFail);
		}
	}

	@PostConstruct
	public void cargarIdAplicacion() throws Exception {
		try {
			List<Aplicacion> listadoApps = new ArrayList<Aplicacion>();
			listadoApps = aplicacion.getAllIdAplicacion();
			AplicacionDTO.getAplicacionDTO(listadoApps);
		} catch (Exception e) {
			ResponseModelConsultarControlesMontosCliente responseWsModelFail = ResponseModelConsultarControlesMontosCliente.builder().build();
			responseWsModelFail.setMsjRespuesta(e.toString());
			logMediacionModel.traceFailMessagesMediation(responseWsModelFail);
		}
	}
}
