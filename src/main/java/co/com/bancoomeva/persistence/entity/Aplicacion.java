package co.com.bancoomeva.persistence.entity;

import java.io.Serializable;
import java.sql.Blob;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "APLICACION")
@Data
@EqualsAndHashCode()
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Aplicacion implements Serializable {

	private static final long serialVersionUID = -3009157732242241601L;

	@Id
	@Column(name = "ID")
	private int id;

	@Column(name = "NOMBRE_APLICACION")
	private String nombreAplicacion;

	@Column(name = "DESCRIPCION_APLICACION")
	private Blob descripcionAplicacion;

	@Column(name = "USUARIO_CREADOR")
	private String usuarioCreador;

	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

	@Column(name = "USUARIO_ACTUALIZADOR")
	private String usuarioActualizador;

	@Column(name = "FECHA_ACTUALIZACION")
	private Date fechaActualizacion;
}
