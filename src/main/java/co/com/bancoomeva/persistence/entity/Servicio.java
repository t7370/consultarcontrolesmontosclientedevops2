package co.com.bancoomeva.persistence.entity;

import java.io.Serializable;
import java.sql.Blob;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "SERVICIO")
@Data
@EqualsAndHashCode()
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Servicio implements Serializable {
	
	private static final long serialVersionUID = -3009157732242241101L;
	
	@Id
	@Column(name = "ID")
	private int id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "OPERACION")
	private String operacion;
	
	@Column(name = "ENDPOINT")
	private String endpoint;
	
	@Column(name = "NOMBRE_EJECUTABLE")
	private String nombreEjecutable;
	
	@Column(name = "DESCRIPCION")
	private Blob descripcion;
	
	@Column(name = "WSDL")
	private String wsdl;
	
	@Column(name = "INSTANCIA")
	private String instancia;
	
	@Column(name = "NOMBRE_SERVIDOR_APLICACIONES")
	private String nombreServidorAplicaciones;
	
	@Column(name = "VERSION_SERVIDOR_APLICACIONES")
	private String versionServidorAplicaciones;
	
	@Column(name = "USUARIO_SERVICIO")
	private String usuarioServicio;
	
	@Column(name = "ID_APLICACION")
	private int idAplicacion;
	
	@Column(name = "ID_PROYECTO")
	private int idProyecto;
	
	@Column(name = "USUARIO_CREADOR")
	private String usuarioCreador;
	
	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;
	
	@Column(name = "USUARIO_ACTUALIZADOR")
	private String usuarioActualizador;
	
	@Column(name = "FECHA_ACTUALIZACION")
	private Date fechaActualizacion;	
}
