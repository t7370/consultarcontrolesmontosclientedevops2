package co.com.bancoomeva.persistence.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode()
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Logs implements Serializable {

	private static final long serialVersionUID = -3009157732242241621L;

	private String messageId;
	private String pasoFlujo;
	private String usuario;
	private String fecha;
	private String contenidoMensaje;
	private String canal;
	private String ip;
	private int idMensajes;
	private int idNivelLog;
	private int idAplicacion;
}
