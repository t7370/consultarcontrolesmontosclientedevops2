package co.com.bancoomeva.persistence.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "MENSAJE")
@Data
@EqualsAndHashCode()
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Mensaje implements Serializable {

	private static final long serialVersionUID = -3009157732242241603L;

	@Id
	@Column(name = "ID")
	private int id;

	@Column(name = "CODIGO")
	private String codigo;

	@Column(name = "CODIGO_BACK")
	private String codigoBack;

	@Column(name = "MENSAJE")
	private String mensaje;

	@Column(name = "USUARIO_CREADOR")
	private String usuarioCreador;

	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

	@Column(name = "USUARIO_ACTUALIZADOR")
	private String usuarioActualizador;

	@Column(name = "FECHA_ACTUALIZACION")
	private Date fechaActualizacion;

	@Column(name = "ID_SERVICIO")
	private int idServicio;

	@Column(name = "ID_TIPO_FLUJO")
	private int idTipoFlujo;
}
