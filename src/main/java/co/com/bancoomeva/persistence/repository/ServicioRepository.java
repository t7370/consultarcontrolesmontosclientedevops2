package co.com.bancoomeva.persistence.repository;

import java.io.Serializable;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import co.com.bancoomeva.persistence.entity.Servicio;

@Transactional
public interface ServicioRepository extends CrudRepository<Servicio, Serializable> {
	@Query(value = "SELECT * FROM MODELO_DATOS_NUBE.SERVICIO "
			+ "WHERE NOMBRE = :nombreServicio limit 1", nativeQuery = true)
	
	public Servicio findByName(@Param("nombreServicio") String nombreServicio);
}
