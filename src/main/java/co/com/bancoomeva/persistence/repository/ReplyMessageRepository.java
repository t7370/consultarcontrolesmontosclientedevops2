package co.com.bancoomeva.persistence.repository;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import co.com.bancoomeva.persistence.dto.MensajeRespuestaDTO;

@Transactional
public interface ReplyMessageRepository extends CrudRepository<MensajeRespuestaDTO, Serializable> {
	@Query( value = "SELECT M.ID, \r\n"
			+ "M.CODIGO,\r\n"
			+ "M.CODIGO_BACK,\r\n"
			+ "M.MENSAJE,\r\n"
			+ "M.ID_SERVICIO,\r\n"
			+ "S.NOMBRE AS NOMBRE_SERVICIO,\r\n"
			+ "S.ID_APLICACION,\r\n"
			+ "A.NOMBRE_APLICACION,\r\n"
			+ "M.ID_TIPO_FLUJO,\r\n"
			+ "T.NOMBRE AS NOMBRE_TIPO_FLUJO\r\n"
			+ "FROM MODELO_DATOS_NUBE.MENSAJE AS M\r\n"
			+ "INNER JOIN MODELO_DATOS_NUBE.SERVICIO AS S ON S.ID = M.ID_SERVICIO\r\n"
			+ "INNER JOIN MODELO_DATOS_NUBE.APLICACION AS A ON A.ID = S.ID_APLICACION\r\n"
			+ "INNER JOIN MODELO_DATOS_NUBE.TIPO_FLUJO AS T ON T.ID = M.ID_TIPO_FLUJO\r\n"
			+ "WHERE S.NOMBRE in (:ServicioCore,:ServicioMediacion)", nativeQuery = true)
	
	public List<MensajeRespuestaDTO> listaMensajes(@Param("ServicioCore") String servicioCore, @Param("ServicioMediacion") String servicioMediacion);
}
