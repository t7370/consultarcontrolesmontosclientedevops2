package co.com.bancoomeva.persistence.repository;

import org.springframework.data.repository.CrudRepository;

import co.com.bancoomeva.persistence.entity.Proyecto;
 
public interface ProjectRepository extends CrudRepository<Proyecto, Long>{
}