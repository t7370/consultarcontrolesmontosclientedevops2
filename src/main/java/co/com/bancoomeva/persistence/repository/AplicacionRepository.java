package co.com.bancoomeva.persistence.repository;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import co.com.bancoomeva.persistence.entity.Aplicacion;

@Transactional
public interface AplicacionRepository extends CrudRepository<Aplicacion, Serializable> {
	@Query(value = "SELECT * FROM APLICACION" + " WHERE NOMBRE_APLICACION = :nombreAplicacion", nativeQuery = true)

	public Aplicacion findByName(@Param("nombreAplicacion") String nombreAplicacion);

	@Query(value = "SELECT * FROM APLICACION" + " WHERE ID = :idAplicacion", nativeQuery = true)
	public Aplicacion findById(@Param("idAplicacion") String idAplicacion);
	
	@Query(value = "SELECT * FROM APLICACION", nativeQuery = true)
	public List<Aplicacion> findAllApp();
}
