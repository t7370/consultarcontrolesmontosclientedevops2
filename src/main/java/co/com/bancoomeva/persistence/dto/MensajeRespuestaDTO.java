package co.com.bancoomeva.persistence.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MENSAJE")
public class MensajeRespuestaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	private int idMensaje;
	
	@Column(name = "CODIGO")
	private String codigo;
	
	@Column(name = "CODIGO_BACK")
	private String codigoBack;
	
	@Column(name = "MENSAJE")
	private String mensaje;

	@Column(name = "ID_SERVICIO")
	private int idServicio;
	
	@Column(name = "NOMBRE_SERVICIO")
	private String nombreServicio;
	
	@Column(name = "ID_APLICACION")
	private int idAplicacion;
	
	@Column(name = "NOMBRE_APLICACION")
	private String nombreAplicacion;
	
	@Column(name = "ID_TIPO_FLUJO")
	private int idTipoFlujo;
	
	@Column(name = "NOMBRE_TIPO_FLUJO")
	private String nombreTipoFlujo;

	


	public MensajeRespuestaDTO() {
		super();
	}


	public MensajeRespuestaDTO(int idMensaje, String codigo, String codigoBack, String mensaje, int idServicio,
			String nombreServicio, int idAplicacion, String nombreAplicacion, int idTipoFlujo, String nombreTipoFlujo) {
		super();
		this.idMensaje = idMensaje;
		this.codigo = codigo;
		this.codigoBack = codigoBack;
		this.mensaje = mensaje;
		this.idServicio = idServicio;
		this.nombreServicio = nombreServicio;
		this.idAplicacion = idAplicacion;
		this.nombreAplicacion = nombreAplicacion;
		this.idTipoFlujo = idTipoFlujo;
		this.nombreTipoFlujo = nombreTipoFlujo;
	}


	public int getIdMensaje() {
		return idMensaje;
	}


	public void setIdMensaje(int idMensaje) {
		this.idMensaje = idMensaje;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getCodigoBack() {
		return codigoBack;
	}


	public void setCodigoBack(String codigoBack) {
		this.codigoBack = codigoBack;
	}


	public String getMensaje() {
		return mensaje;
	}


	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


	public int getIdServicio() {
		return idServicio;
	}


	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}


	public String getNombreServicio() {
		return nombreServicio;
	}


	public void setNombreServicio(String nombreServicio) {
		this.nombreServicio = nombreServicio;
	}


	public int getIdAplicacion() {
		return idAplicacion;
	}


	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}


	public String getNombreAplicacion() {
		return nombreAplicacion;
	}


	public void setNombreAplicacion(String nombreAplicacion) {
		this.nombreAplicacion = nombreAplicacion;
	}


	public int getIdTipoFlujo() {
		return idTipoFlujo;
	}


	public void setIdTipoFlujo(int idTipoFlujo) {
		this.idTipoFlujo = idTipoFlujo;
	}


	public String getNombreTipoFlujo() {
		return nombreTipoFlujo;
	}


	public void setNombreTipoFlujo(String nombreTipoFlujo) {
		this.nombreTipoFlujo = nombreTipoFlujo;
	}	

	
}
