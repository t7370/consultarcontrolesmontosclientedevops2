package co.com.bancoomeva.persistence.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Esta clase define la respuesta de la entidad
 * 
 * @author JhonCTorres
 * @version 04/01/2021
 */
public class MensajeRespuestaObject implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private List<MensajeRespuestaDTO> listadoMensajes;
	
	private static MensajeRespuestaObject miMensajeRespuestaObject;

	public static MensajeRespuestaObject getMensajeRespuestaDTO(List<MensajeRespuestaDTO> listadoMensajes) throws Exception {
		
		if(miMensajeRespuestaObject==null) {
			miMensajeRespuestaObject = new MensajeRespuestaObject(listadoMensajes);
		}
		return miMensajeRespuestaObject;				
	}
	
	

	private MensajeRespuestaObject(List<MensajeRespuestaDTO> listadoMensajes) {
		super();
		this.listadoMensajes = listadoMensajes;
	}



	public List<MensajeRespuestaDTO> getListadoMensajes() {
		return listadoMensajes;
	}

	public void setListadoMensajes(List<MensajeRespuestaDTO> listadoMensajes) {
		this.listadoMensajes = listadoMensajes;
	}
	
	
	
}
