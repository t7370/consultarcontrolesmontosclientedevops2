package co.com.bancoomeva.persistence.dto;

import java.io.Serializable;
import java.util.List;

import co.com.bancoomeva.persistence.entity.Aplicacion;

/**
 * Esta clase define la respuesta de la entidad
 * 
 * @author JhonCTorres
 * @version 04/01/2021
 */
public class AplicacionDTO implements Serializable {
	
	private static final long serialVersionUID = -696202764144342853L;

	private List<Aplicacion> listadoApps;	
	private static AplicacionDTO miAplicacionDTO;

	public static AplicacionDTO getAplicacionDTO(List<Aplicacion> listadoApps) throws Exception {		
		if(miAplicacionDTO==null) {
			miAplicacionDTO = new AplicacionDTO(listadoApps);
		}
		return miAplicacionDTO;				
	}
	
	private AplicacionDTO(List<Aplicacion> listadoApps) {
		super();
		this.listadoApps = listadoApps;
	}

	public List<Aplicacion> getListadoApps() {
		return listadoApps;
	}

	public void setListadoApps(List<Aplicacion> listadoApps) {
		this.listadoApps = listadoApps;
	}	
	
}
