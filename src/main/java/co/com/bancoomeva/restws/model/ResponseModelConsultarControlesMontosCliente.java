package co.com.bancoomeva.restws.model;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data 
@AllArgsConstructor 
@NoArgsConstructor 
@Builder
@ToString
@EqualsAndHashCode
public class ResponseModelConsultarControlesMontosCliente {
	@ApiModelProperty(notes = "Codigo de respuesta del servicio de la mediacion", example = "593", required = true, position = 1)
	private String codRespuesta;
	
	@ApiModelProperty(notes = "Mensaje de respuesta del servicio de la mediacion", example = "Operacion exitosa", required = true, position = 2)
	private String msjRespuesta;
	
	@ApiModelProperty(notes = "Limite diario del servicio de la mediacion", example = "Operacion exitosa", required = true, position = 3)
	private String limiteDiarioBco;
	
	@ApiModelProperty(notes = "Limite transacción del servicio de la mediacion", example = "Operacion exitosa", required = true, position = 4)
	private String limiteTransaccionBco;
	
	@ApiModelProperty(notes = "Arreglo de cuentas del cliente encontradas en la consulta", example = "", required = true, position = 5)
	private List<ConsultarControlesMontosCliente> ListaconsultarControlesMontosCliente;
	
}
