package co.com.bancoomeva.restws.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data 
@AllArgsConstructor 
@NoArgsConstructor 
@Builder
@ToString
@EqualsAndHashCode
public class ConsultarControlesMontosCliente {

	private String transaccion;
	private String nombre;
	private String montoMaxDia;
	private String montoMaxOperacion;
	private String estado;
	private String usuarioUltModificacion;
	private String fechaUltModificacion;
	private String horaUltModificacion;
}
