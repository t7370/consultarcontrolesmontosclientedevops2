package co.com.bancoomeva.restws.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data 
@AllArgsConstructor 
@NoArgsConstructor 
@Builder
@ToString
@EqualsAndHashCode
public class RequestModelConsultBranchCity {

	@ApiModelProperty(notes = "Compuesto del prefijo que identifica a la banca movil (BNCMV-) y una secuencia numerica", example = "BNCMV-################", required = true, position = 1)
	private String messageId;
	
	@ApiModelProperty(notes = "Fecha y hora de la transaccion", example = "YYYY-MM-DDThh:mm:ss", required = true, position = 2)
	private String invokerDateTime;
	
	@ApiModelProperty(notes = "IP del celular del cliente, debe corresponder con la IP que aparece en la aplicacion de cada cliente", example = "xxx.xxx.xxx.xxx", required = true, position = 3)
	private String ipTransaccion;
	
	@ApiModelProperty(notes = "Codigo unico para este tipo de transaccion", example = "123456789", required = true, position = 4)
	private String codTransaccion;
	
	@ApiModelProperty(notes = "Identificador del canal utilizado para la transaccion", example = "1= MB (Mobile Banking) y 2= OV (Oficina Virtual)", required = true, position = 5)
	private String canal;
	
	@ApiModelProperty(notes = "El nombre del usuario que se encuentra logueado en la aplicacion. Este campo no lo tiene el CORE Banco.", example = "jbermudez", required = true, position = 6)
	private String usuario;
	
	@ApiModelProperty(notes = "Identificador de la aplicacion que consume el servicio de la mediacion", example = "4", required = true, position = 7)
	private String idAplicacion;
	
	@ApiModelProperty(notes = "Identificador del tipo de documento del cliente", example = "1 – Cedula. 2 – Cedula Extra 3 – Registro Civil  4 – Tarjeta ID 5 – Pasaporte", required = true, position = 8)	
	private String tipoDocumento;
	
	@ApiModelProperty(notes = "Numero de identificacion del cliente", example = "31799899", required = true, position = 9)
	private String numDocumento;
	
	@ApiModelProperty(notes = "Codigo del usuario ", example = "31799899", required = true, position = 10)
	private String usuarioBack;
	
	
}
