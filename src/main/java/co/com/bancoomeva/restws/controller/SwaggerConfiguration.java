package co.com.bancoomeva.restws.controller;

import com.google.common.base.Predicate;

import co.com.bancoomeva.constants.Constants;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Esta clase expone el endpoind de swagger
 * 
 * @author JhonCTorres
 * @version 28/12/2020
 *
 */
@EnableSwagger2
@Configuration
public class SwaggerConfiguration {

	/**
	 * 
	 * Metodo que publica el api de swagger
	 * 
	 * @return a swagger configuration bean
	 */
	@Bean
	public Docket usersApi() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(usersApiInfo()).select().paths(userPaths())
				.apis(RequestHandlerSelectors.basePackage("co.com.bancoomeva.restws.controller")).build()
				.useDefaultResponseMessages(false);
	}

	private ApiInfo usersApiInfo() {
		return new ApiInfoBuilder().title(Constants.API_INFO_TITLE + "Consultar Controles Montos Cliente").version("1.0")
				.license("Apache License Version 2.0").build();
	}

	private Predicate<String> userPaths() {
		return regex("/.*");
	}
}
