package co.com.bancoomeva.restws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.bancoomeva.constants.Constants;
import co.com.bancoomeva.restws.business.IConsultarControlesMontosCliente;
import co.com.bancoomeva.restws.business.ILogMediacion;
import co.com.bancoomeva.restws.model.RequestModelConsultBranchCity;
import co.com.bancoomeva.restws.model.ResponseModelConsultarControlesMontosCliente;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Esta clase define el servicio y los metodos que los componen
 * 
 * @author JhonCTorres
 * @version 05/04/2021
 */
@RestController
@RequestMapping(path = "mediacionewallet/api/v1")
public class MediationConsultarControlesMontosCliente {

	private final IConsultarControlesMontosCliente consultarControlesMontosCliente;
	
	@Autowired
	ILogMediacion logMediacionModel;
	
	public MediationConsultarControlesMontosCliente(IConsultarControlesMontosCliente clientWsTransaccion) {
		this.consultarControlesMontosCliente= clientWsTransaccion;
	}
	
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = Constants.CONSULT_SUCCESSFUL, response = ResponseModelConsultarControlesMontosCliente.class),
			@ApiResponse(code = 500, message = Constants.INTERNAL_SERVER_ERROR),
			@ApiResponse(code = 503, message = Constants.SERVICE_NOT_AVAILABLE),
			@ApiResponse(code = 598, message = Constants.XML_STRUCTURE_ERROR),
			@ApiResponse(code = 592, message = Constants.CLIENT_NOT_FOUND),
			@ApiResponse(code = 563, message = Constants.RECORDS_NOT_FOUND),
			@ApiResponse(code = 569, message = Constants.ERROR_VALIDATING_FIELDS)	
	})
	@ApiOperation(value = Constants.CONSULT_CREAR_CLIENTES_DATA, notes = Constants.CONSULT_CREAR_CLIENTES_CATS_NOTE)
	@PostMapping(value = "/consultarControlesMontosCliente", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> crearClientesCats(@RequestBody RequestModelConsultBranchCity mediationRequest) throws Exception {
		try {
			return consultarControlesMontosCliente.consultBranchCity(mediationRequest);
		}catch (Exception e) {
			return ResponseEntity.status(503).body(Constants.SERVICE_NOT_AVAILABLE+ e);
		}
	}

}
