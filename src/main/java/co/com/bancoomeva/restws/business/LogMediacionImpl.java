package co.com.bancoomeva.restws.business;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.bancoomeva.backendws.wsbeans.Bnkws023;
import co.com.bancoomeva.backendws.wsbeans.Bnkws023Response;
import co.com.bancoomeva.constants.Constants;
import co.com.bancoomeva.persistence.dto.MensajeRespuestaDTO;
import co.com.bancoomeva.persistence.entity.Logs;
import co.com.bancoomeva.restws.model.RequestModelConsultBranchCity;
import co.com.bancoomeva.restws.model.ResponseModelConsultarControlesMontosCliente;
import co.com.bancoomeva.util.BuildLog;
import co.com.bancoomeva.util.ReadInformationInitial;
import co.com.bancoomeva.util.Utils;

/**
 * Esta clase define la estructura para el LOG hacia la BD
 * 
 * @author JhonCTorres
 * @version 04/01/2021
 */
@Component
public class LogMediacionImpl implements ILogMediacion {

	private static final Logger LOGGER = LoggerFactory.getLogger(LogMediacionImpl.class);
	private static final BigDecimal SUCCESS = new BigDecimal(0);
	private static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";

	ObjectMapper obj = new ObjectMapper();
	
	
	String channel;
	RequestModelConsultBranchCity tempMediationRequest;

//	String codigo;
	String userPetition = "";
	int idApp = 0;
	
	
	@Value("${query.nivelLog}")
	int nivelLog;

	/**
	 * Metodo que se encarga de guardar el LOG REQUEST
	 */
	public void traceLogWsRequest(RequestModelConsultBranchCity request) throws Exception {
		String answerLog = obj.writeValueAsString(BuildLog.buildLog(request, Constants.PETITION_MEDIATION_INITIAL));
		LOGGER.info(answerLog);

	}
	
	public void traceLogWsRequestIdAppFail(RequestModelConsultBranchCity validateSendSMSRequest, String nombreFlujo,
			String nombreAplicacion) throws Exception {
		Date date = new Date();
		SimpleDateFormat formato = new SimpleDateFormat(DATEFORMAT);
		String fecha = formato.format(date);
		tempMediationRequest = validateSendSMSRequest;
		userPetition = validateSendSMSRequest.getUsuario();
		String answerLog = "";
		ReadInformationInitial infoMensaje = ReadInformationInitial.builder().build();
		int idMessage = infoMensaje.getIdMensaje(nombreFlujo, nombreAplicacion);
		channel = validateSendSMSRequest.getCanal();
		String value = obj.writeValueAsString(validateSendSMSRequest);
		Logs saveLog = Logs.builder().build();
		saveLog.setMessageId(validateSendSMSRequest.getMessageId());
		saveLog.setPasoFlujo(Constants.PETITION_MEDIATION_INITIAL);
		saveLog.setContenidoMensaje(value);
		saveLog.setCanal(channel);
		saveLog.setUsuario(userPetition);
		saveLog.setFecha(fecha);
		saveLog.setIp(validateSendSMSRequest.getIpTransaccion());
		saveLog.setIdMensajes(idMessage);
		saveLog.setIdNivelLog(nivelLog);
		answerLog = obj.writeValueAsString(saveLog);
		LOGGER.info(answerLog);
		
	}

	public String traceLogWsRequestFail(String error) throws Exception {
		MensajeRespuestaDTO mensajeRespuestaFail = Utils.consultMessage(Constants.ERROR_500, Constants.RESPONSE, Constants.MEDIADOREKS);
		ResponseModelConsultarControlesMontosCliente responseWsFail = ResponseModelConsultarControlesMontosCliente.builder().build();
		responseWsFail.setCodRespuesta(mensajeRespuestaFail.getCodigo());
		responseWsFail.setMsjRespuesta(mensajeRespuestaFail.getMensaje() + " - " + error);
		return traceLogResponseWs(responseWsFail, Constants.MEDIADOREKS);
	}

	/**
	 * Metodo que se encarga de guardar el LOG REQUEST TAYLOR
	 */
	public void traceLogWsTaylor(Bnkws023 request) throws Exception {
		String respuestaLog = obj.writeValueAsString(BuildLog.buildLog(request, Constants.PETITION_CORE_TAYLOR));
		LOGGER.info(respuestaLog);
	}

	/**
	 * Metodo que se encarga de guardar el LOG RESPONSE TAYLOR
	 */
	public void traceLogResponseTaylor(Bnkws023Response response) throws Exception {
		String respuestaLog = "";
		Logs saveLog = BuildLog.buildLog(response, Constants.PETITION_TAYLOR_RESPONSE);
		respuestaLog = obj.writeValueAsString(saveLog);
		if (!response.getReturn().getOCODRES().equals(SUCCESS)) {
			LOGGER.error(respuestaLog);
		} else {
			LOGGER.info(respuestaLog);
		}

	}

	/**
	 * Metodo que se encarga de guardar el LOG RESPONSE MEDIACION
	 */
	public String traceLogResponseWs(ResponseModelConsultarControlesMontosCliente response, String app) throws Exception {
		String respuestaLog = "";
		Logs saveLog = BuildLog.buildLog(response, Constants.PETITION_MEDIATION_RESPONSE, app);
		respuestaLog = obj.writeValueAsString(saveLog);
		if (!response.getCodRespuesta().equals(Constants.SUCCESS)) {
			LOGGER.error(respuestaLog);
		} else {
			LOGGER.info(respuestaLog);
		}
		return saveLog.getContenidoMensaje();

	}

	public void traceFailMessagesMediation(ResponseModelConsultarControlesMontosCliente contenidoPeticion) throws Exception {
		String valueFail = contenidoPeticion.getMsjRespuesta();
		MensajeRespuestaDTO mensajeRespuestaFail = Utils.consultMessage(Constants.ERROR_500, Constants.RESPONSE, Constants.MEDIADOREKS);
		ResponseModelConsultarControlesMontosCliente responseWsFail = ResponseModelConsultarControlesMontosCliente.builder().build();
		responseWsFail.setCodRespuesta(Constants.ERROR_500);
		responseWsFail.setMsjRespuesta(mensajeRespuestaFail.getMensaje() + " - " + valueFail);
		traceLogResponseWs(responseWsFail, Constants.MEDIADOREKS);
	}

}
