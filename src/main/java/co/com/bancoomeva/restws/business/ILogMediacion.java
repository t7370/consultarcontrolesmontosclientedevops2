package co.com.bancoomeva.restws.business;

import co.com.bancoomeva.backendws.wsbeans.Bnkws023;
import co.com.bancoomeva.backendws.wsbeans.Bnkws023Response;
import co.com.bancoomeva.restws.model.RequestModelConsultBranchCity;
import co.com.bancoomeva.restws.model.ResponseModelConsultarControlesMontosCliente;

/**
 * Esta clase define la interfaz del LOG y los metodos que los componen
 * 
 * @author JhonCTorres
 * @version 05/04/2021
 */
public interface ILogMediacion {
	
	public void traceLogWsRequestIdAppFail(RequestModelConsultBranchCity requestWs, String nombreFlujo, String nombreAplicacion)
			throws Exception;

	public void traceLogWsRequest(RequestModelConsultBranchCity requestWs) throws Exception;

	public String traceLogWsRequestFail(String error) throws Exception;

	public void traceLogWsTaylor(Bnkws023 peticionTaylor) throws Exception;

	public void traceLogResponseTaylor(Bnkws023Response response) throws Exception;

	public String traceLogResponseWs(ResponseModelConsultarControlesMontosCliente responseWsModel, String app) throws Exception;

	public void traceFailMessagesMediation(ResponseModelConsultarControlesMontosCliente responseWsModel) throws Exception;

}
