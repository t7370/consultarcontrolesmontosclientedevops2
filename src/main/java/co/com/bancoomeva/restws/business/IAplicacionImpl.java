package co.com.bancoomeva.restws.business;

import co.com.bancoomeva.persistence.entity.Aplicacion;

/**
 * Esta clase define la interfaz del servicio y los metodos que los componen
 * 
 * @author JhonCTorres
 * @version 04/01/2021
 */
public interface IAplicacionImpl {
	public Aplicacion getIdAplicacion(String nombreAplicacion) throws Exception;

	public Aplicacion validateId(int idAplicacion) throws Exception;

}
