package co.com.bancoomeva.restws.business;

import org.springframework.http.ResponseEntity;

import co.com.bancoomeva.restws.model.RequestModelConsultBranchCity;

public interface IConsultarControlesMontosCliente {
	public ResponseEntity<String> consultBranchCity(RequestModelConsultBranchCity requestWs) throws Exception;
}
