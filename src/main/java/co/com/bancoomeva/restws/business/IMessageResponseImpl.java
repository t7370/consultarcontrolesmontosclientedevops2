package co.com.bancoomeva.restws.business;

import java.util.List;

import co.com.bancoomeva.persistence.dto.*;

/**
 * Esta clase define la interfaz del LOG y los metodos que los componen
 * 
 * @author JhonCTorres
 * @version 04/01/2021
 */
public interface IMessageResponseImpl {
	
	public List<MensajeRespuestaDTO> listaMensajesInterfaz() throws Exception;
	
}
