package co.com.bancoomeva.restws.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.bancoomeva.persistence.entity.Aplicacion;
import co.com.bancoomeva.persistence.repository.AplicacionRepository;

/**
 * Esta clase define la implementacion del servicio
 * 
 * @author JhonCTorres
 * @version 04/01/2021
 */
@Service
public class AplicacionImpl implements IAplicacionImpl{
	@Autowired
	private AplicacionRepository appRepository;
	
	public Aplicacion getIdAplicacion(String nombreAplicacion) throws Exception {
		 
		return appRepository.findByName(nombreAplicacion);
	}
	
	public Aplicacion validateId(int idAplicacion) throws Exception {
		 
		return appRepository.findById(String.valueOf(idAplicacion));
	}
	
	public List<Aplicacion> getAllIdAplicacion() throws Exception {
		return appRepository.findAllApp();
	}
}
