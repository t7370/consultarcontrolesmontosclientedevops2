package co.com.bancoomeva.restws.business;

import co.com.bancoomeva.persistence.entity.Servicio;

/**
 * Esta clase define la interfaz del servicio y los metodos que los componen
 * 
 * @author JhonCTorres
 * @version 04/01/2021
 */
public interface IServicio{
	
	public Servicio getServicio(String service) throws Exception;

}
