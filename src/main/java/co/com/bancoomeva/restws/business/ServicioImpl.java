package co.com.bancoomeva.restws.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.bancoomeva.persistence.entity.Servicio;
import co.com.bancoomeva.persistence.repository.ServicioRepository;

/**
 * Esta clase define la implementacion del servicio
 * 
 * @author JhonCTorres
 * @version 04/01/2021
 */
@Service
public class ServicioImpl implements IServicio{
	@Autowired
	private ServicioRepository sevicioRepository;
	
	public Servicio getServicio(String service) throws Exception {
		return sevicioRepository.findByName(service);
	}

}
