package co.com.bancoomeva.restws.business;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;

import co.com.bancoomeva.backendws.wsbeans.BNKWS023Result;
import co.com.bancoomeva.backendws.wsbeans.Bnkws023;
import co.com.bancoomeva.backendws.wsbeans.Bnkws023Response;
import co.com.bancoomeva.constants.Constants;
import co.com.bancoomeva.enums.MediationService;
import co.com.bancoomeva.persistence.dto.MensajeRespuestaDTO;
import co.com.bancoomeva.restws.model.RequestModelConsultBranchCity;
import co.com.bancoomeva.restws.model.ResponseModelConsultarControlesMontosCliente;
import co.com.bancoomeva.util.MappingParameters;
import co.com.bancoomeva.util.Utils;

@Component
public class ConsultarControlesMontoCliente implements IConsultarControlesMontosCliente {

	private final WebServiceTemplate transaccionWsResource;

	@Autowired
	ILogMediacion logMediacionModel;

	int nivelLog;

	public ConsultarControlesMontoCliente(WebServiceTemplate transaccionWs) {
		this.transaccionWsResource = transaccionWs;
	}

	@Override
	public ResponseEntity<String> consultBranchCity(RequestModelConsultBranchCity requestWs) throws Exception {
		String respuestaMediacion = "";
		Bnkws023Response responseTransaccionesXml = null;
		nivelLog = MediationService.CONSULTAR_CONTROLES_MONTOS.getNivelLog();
		try {
			logMediacionModel.traceLogWsRequest(requestWs);

			Bnkws023 requestTransaccionesXml = MappingParameters.mapMediationToTaylor(requestWs);

			if (nivelLog >= 2) {
				logMediacionModel.traceLogWsTaylor(requestTransaccionesXml);
			}
			try {
				responseTransaccionesXml = (Bnkws023Response) transaccionWsResource
						.marshalSendAndReceive(requestTransaccionesXml);
				if (nivelLog >= 2) {
					logMediacionModel.traceLogResponseTaylor(responseTransaccionesXml);
				}
				ResponseModelConsultarControlesMontosCliente responseWsModel = MappingParameters
						.mapTaylorToMediation(responseTransaccionesXml);
				respuestaMediacion = logMediacionModel.traceLogResponseWs(responseWsModel, Constants.TAYLOR);
			} catch (WebServiceIOException e) {
				String errorServicio = e.getMessage();
				String code = Constants.ERROR_503;
				respuestaMediacion = logMediacionModel.traceLogResponseWs(errorResponse(code, errorServicio), Constants.MEDIADOREKS);
			} catch (Exception e) {
				String errorServicio = e.getMessage();
				String code = Constants.ERROR_500;
				respuestaMediacion = logMediacionModel.traceLogResponseWs(errorResponse(code, errorServicio), Constants.MEDIADOREKS);
				
			}
		} catch (Exception e) {
			logMediacionModel.traceLogWsRequestIdAppFail(requestWs, Constants.REQUEST,Constants.MEDIADOREKS);
			respuestaMediacion = logMediacionModel.traceLogWsRequestFail(e.getMessage());
		}
		return ResponseEntity.ok(respuestaMediacion);
	}

	public ResponseModelConsultarControlesMontosCliente errorResponse(String code, String error) throws Exception {
		Bnkws023Response responseTransaccionesXmlFail = new Bnkws023Response();
		BNKWS023Result wsCreateClientResultFail = new BNKWS023Result();
		ResponseModelConsultarControlesMontosCliente responseWsModelFail = ResponseModelConsultarControlesMontosCliente.builder().build();
		MensajeRespuestaDTO mensajeRespuestaFail = Utils.consultMessage(code, Constants.RESPONSE, Constants.MEDIADOREKS);
		wsCreateClientResultFail.setOMENRES(mensajeRespuestaFail.getMensaje());
		wsCreateClientResultFail.setOCODRES(new BigDecimal(mensajeRespuestaFail.getCodigoBack()));
		responseTransaccionesXmlFail.setReturn(wsCreateClientResultFail);

		responseWsModelFail.setCodRespuesta(mensajeRespuestaFail.getCodigo());
		responseWsModelFail.setMsjRespuesta(mensajeRespuestaFail.getMensaje() + " - " + error);
		if (nivelLog >= 2) {
			logMediacionModel.traceLogResponseTaylor(responseTransaccionesXmlFail);
		}
		return responseWsModelFail;
	}
}
