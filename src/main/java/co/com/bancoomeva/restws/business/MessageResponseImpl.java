package co.com.bancoomeva.restws.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import co.com.bancoomeva.persistence.dto.MensajeRespuestaDTO;
import co.com.bancoomeva.persistence.repository.ReplyMessageRepository;

/**
 * Esta clase define la implementacion del servicio y los metodos que los componen
 * 
 * @author JhonCTorres
 * @version 06/12/2020
 */
@Service
public class MessageResponseImpl implements IMessageResponseImpl{
	@Autowired
	private ReplyMessageRepository MRespuestaRepository;
	@Value("${query.servicio_core}") String dato1;
	@Value("${query.servicio_mediacion}") String dato2;
	@Override
	public List<MensajeRespuestaDTO> listaMensajesInterfaz() throws Exception {
		 
		return MRespuestaRepository.listaMensajes( dato1,dato2);
	}


}
