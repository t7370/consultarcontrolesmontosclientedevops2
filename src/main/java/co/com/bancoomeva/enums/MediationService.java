package co.com.bancoomeva.enums;

import co.com.bancoomeva.util.ConfigurationProperties;
import co.com.bancoomeva.util.StaticContextAccessor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum MediationService {

	CONSULTAR_CONTROLES_MONTOS(ConstantService.SERVICE_CORE, ConstantService.SERVICE_MEDIATION, ConstantService.TIME_OUT,
			ConstantService.NIVEL_LOG);

	@Getter
	private String serviceCore;
	@Getter
	private String serviceName;
	@Getter
	private int timeout;
	@Getter
	private int nivelLog;

	private MediationService(String serviceCore, String serviceName, int timeout, int nivelLog) {
		this.serviceCore = serviceCore;
		this.serviceName = serviceName;
		this.timeout = timeout;
		this.nivelLog = nivelLog;
	}

	private static class ConstantService {
		private static ConfigurationProperties configurationProperties = StaticContextAccessor
				.getBean(ConfigurationProperties.class);
		private static final String SERVICE_MEDIATION = configurationProperties.getServiceMediation();
		private static final String SERVICE_CORE = configurationProperties.getServiceCore();
		private static final int TIME_OUT = configurationProperties.getTimeout();
		private static final int NIVEL_LOG = configurationProperties.getNivelLog();

		private ConstantService() {
			throw new IllegalStateException("ConstantService class");
		}
	}
}
