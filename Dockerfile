FROM alpine:3.15.0

USER root

## Los archivos descargados se ubicaran en /tmp para ser borrados al final
WORKDIR /tmp

## Actualizando alpine
RUN apk update &&  apk upgrade

## Instalar openjdk alpine
RUN apk fetch openjdk8
RUN apk add openjdk8

## Descargar tomcat 8.5
RUN wget https://downloads.apache.org/tomcat/tomcat-8/v8.5.81/bin/apache-tomcat-8.5.81.tar.gz 

## Instalar tomcat 8.5
RUN tar -zxvf /tmp/apache-tomcat-8.5.81.tar.gz -C /usr/local
RUN cd /usr/local && mv apache-tomcat-8.5.81 tomcat

## Eliminar instaladores descargados
RUN cd /tmp && rm *

## Exponer puerto
EXPOSE 8080

##  path-to-your-application-war path-to-webapps-in-docker-tomcat
COPY target/consultarControlesMontosCliente-1.0.0.war /usr/local/tomcat/webapps/consultarControlesMontosCliente.war

## Iniciar tomcat
CMD sh /usr/local/tomcat/bin/catalina.sh start && tail -f /usr/local/tomcat/logs/catalina.out
